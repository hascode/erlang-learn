-module(url).
-export([urlToDomain/1]).

urlToDomain(Url) -> 
 "http://" ++ Domain = Url,
 Domain.
