-module(exceptions).
-export([sample/0, sample2/0]).

sample() ->
 try
   throwSomeError()
 catch
   error:Reason -> io:format("error ~p", [Reason]);
   _:_ -> io:format("some unknown exception")
 end
.

throwSomeError() ->
 error("Just because") 
.

sample2() ->
 {ok, FileHandle} = file:open("exceptions.erl", [read]),
try
 io:format("doing sumthing with file")
after
 file:close(FileHandle)
end
.
