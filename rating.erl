-module(rating).
-export([rate_by_point/1, points_for_rating/1 ]).


%%% rating by given points
rate_by_point(Points) when Points >= 0, Points =< 100 ->

Title = if
 Points < 25 -> reallyBad;
 Points < 50 -> bad;
 Points < 75 -> ok;
 Points < 100 -> good 
end,

Title.


%%% points for rating
points_for_rating(Rating) ->

 Points = case Rating of
  reallyBad -> 24;
  bad       -> 49;
  ok        -> 74;
  _         -> 99
 end,
Points. 
