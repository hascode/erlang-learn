%%
%% returns even elements from a list of numbers
%% usage: comprehensions:sample1([1 | [2,3,4]]).
%%

-module(comprehensions).
-export([sample1/1]).

sample1(Nums) when is_list(Nums) ->
  Even = [X || X <- Nums, X rem 2 =:= 0],
  Even
.


