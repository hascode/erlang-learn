-module(shape).
-export([area/1, area2/1]).

area({rectangle, Width, Height}) ->
 Width * Height;

area({circle, Radius}) ->
 Radius * Radius * math:pi();

area({square, Side}) ->
 Side * Side
.

area2(Shape) ->
 case Shape of
  {rectangle, Width, Height} ->
    Width * Height;
  {circle, Radius} ->
    Radius * Radius * math:pi();
  {square, Side} -> 
    Side * Side
end.
