-module(calc).
-export([fib/1]).

fib(Number) when Number == 0 -> 1;
fib(Number) when Number > 0 -> Number*fib(Number-1).
