-module(bitstream).
-export([sample1/1]).

sample1(<<Version:2, Signal:2, Content>>) ->
  io:format("input received - version: ~w, signal: ~w. content: ~w",[Version, Signal, Content])
.
