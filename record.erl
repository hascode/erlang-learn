%%
%% usage: record:sample1(#user{name="Fred", id=123}).
%%
-module(record).
-export([sample1/1]).
-record(user, {name="anonymous", id}).

sample1(#user{name=Name, id=Id}) ->
 io:format("User has name ~w and id ~w", [Name, Id])
.
